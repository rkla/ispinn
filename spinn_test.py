import cppimport.import_hook
import spinn

import numpy as np


def test_lif_sim_():
    num_neurons = 10
    weight_matrix = np.ones((num_neurons, num_neurons))
    delay_matrix = 2*np.ones((num_neurons, num_neurons))
    ext_input = np.arange(num_neurons)
    lifsimobj = spinn.LIFSimulation(num_neurons,
                                       0.1,
                                       weight_matrix,
                                       delay_matrix,
                                       0,
                                       1,
                                       0,
                                       None,
                                       ext_input,
                                       1,
                                       0.1,
                                       0.5,
                                       0,
                                       2,
                                       1,
                                       2);
    assert lifsimobj.num_neurons == num_neurons
    assert (lifsimobj.delay_matrix == delay_matrix).all()
    assert (lifsimobj.weight_matrix == weight_matrix).all()
    assert len(lifsimobj.step()) == 3
    for i in range(3):
        assert isinstance(lifsimobj.step()[i], np.ndarray)
    lifsimobj.add_input(ext_input)


if __name__ == '__main__':
    test_lif_sim_()
    print("Test successful.")