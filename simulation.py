from typing import Tuple, Union
import time

import numba
from numba.typed import List

import numpy as np
from numpy import ndarray
from scipy.spatial.distance import cdist
import scipy.sparse as sparse


# from line_profiler_pycharm import profile


class LIFSimulation:
    def __init__(self, num_neurons: int,
                 dt: float,
                 weight_matrix: np.ndarray,
                 delay_matrix: np.ndarray,
                 v_r: Union[float, list, np.ndarray],
                 v_t: Union[float, list, np.ndarray],
                 v_l: Union[float, list, np.ndarray],
                 v0: np.ndarray = None,
                 ext_input: np.ndarray = None,
                 tau_ref: Union[int, list, np.ndarray] = 1,
                 tau_mem: float = 1.,
                 cap_mem: float = 1.,
                 synaptic_kernel: int = 0,
                 tau_exc: float = 1.,
                 tau_inh: float = 1.,
                 tau_cutoff: float = 1.,
                 use_sparse=False):
        self.num_neurons = num_neurons
        self.dt = dt
        self.tau_ref = tau_ref
        self.tau_mem = tau_mem
        self.cap_mem = cap_mem
        self.synaptic_kernel = synaptic_kernel
        self.tau_exc = tau_exc
        self.tau_inh = tau_inh
        self.tau_cutoff = tau_cutoff

        if v0 is None:
            self.v = np.zeros(num_neurons)
        else:
            self.v = v0

        if isinstance(v_r, (list, np.ndarray)):
            self.v_reset = v_r
        else:
            self.v_reset = v_r * np.ones(num_neurons)

        if isinstance(v_t, (list, np.ndarray)):
            self.v_thresh = v_t
        else:
            self.v_thresh = v_t * np.ones(num_neurons)

        if isinstance(v_l, (list, np.ndarray)):
            self.v_leak = v_l
        else:
            self.v_leak = v_l * np.ones(num_neurons)

        self.mouse_input = np.zeros(self.num_neurons)

        # print(sparse.isspmatrix_lil(delay_matrix))
        if self.synaptic_kernel == 0:
            self.max_delay = int(np.max(delay_matrix))
        elif self.synaptic_kernel == 1:
            self.max_delay = int(max(np.max(delay_matrix), max(self.tau_cutoff * self.tau_exc, self.tau_cutoff * self.tau_inh)))
        self.delay_matrix = delay_matrix.astype(np.int32)
        self.weight_matrix = weight_matrix.astype(np.float32)
        if use_sparse:
            self.connection_inds = weight_matrix.nonzero()
            self.delay_matrix = delay_matrix.tolil()
            self.weight_matrix = weight_matrix.tolil()
        else:
            self.connection_mask = weight_matrix != 0

        self.refractory = np.zeros(num_neurons)

        self.spike_ring_buffer = List([List([List([0.0, 0.0])]) for _ in range(self.max_delay + 1)])
        self.full_targets = np.repeat(np.arange(num_neurons), num_neurons).reshape(num_neurons, num_neurons)
        self.spike_mask_cols = np.zeros((num_neurons, num_neurons), np.bool_)
        self.current_buffer_pos = 0

        self.delay_cache = []
        self.weight_cache = []
        self.input_cache = []
        if ext_input is not None:
            self.input_cache.append(ext_input)
        self.it = 0

        self.neuron_record = [None, None, None]

    def add_input(self, inp):
        self.input_cache.append(inp)

    def step(self, **kwargs) -> np.ndarray:
        if kwargs["use_numba"]:
            return self.step_numba_list()

        # external current signal
        new_input_cache = []
        external_input = np.zeros(self.num_neurons)
        for ext_inp in self.input_cache:
            external_input += ext_inp[:, 0]
            if ext_inp.shape[1] > 1:
                ext_inp = ext_inp[:, 1:]
                new_input_cache.append(ext_inp)
        self.input_cache = new_input_cache

        # spike mechanism
        spike_mask = np.logical_and(self.v >= self.v_thresh,
                                    np.logical_not(self.refractory > 0))
        self.v[spike_mask] = self.v_reset[spike_mask]
        self.refractory[spike_mask] = self.tau_ref

        # delay processing
        if spike_mask.any():
            ind_cols = np.s_[:, spike_mask]
            delay_entry = self.delay_matrix[ind_cols]
            self.delay_cache.append(delay_entry)
            weight_entry = self.weight_matrix[ind_cols]
            self.weight_cache.append(weight_entry)

        next_delay_cache = []
        next_weight_cache = []
        spike_input = np.zeros(self.num_neurons)
        for i_del, delay in enumerate(self.delay_cache):
            np.add(delay, -1, out=delay)
            # arrived_spikes = np.where(delay == 0, weight_cache[i_del], np.zeros_like(delay))  # 18% !!
            # spike_input += np.sum(arrived_spikes, axis=1)  # 8%
            arrived_mask = delay == 0
            weight_input = np.sum(arrived_mask * self.weight_cache[i_del], axis=1)
            np.add(spike_input, weight_input, out=spike_input)

            if (delay > 0).any():
                next_delay_cache.append(delay)
                next_weight_cache.append(self.weight_cache[i_del])
        self.delay_cache = next_delay_cache
        self.weight_cache = next_weight_cache

        # forward Euler integration
        current = external_input + spike_input
        refractory_mask = self.refractory > 0
        update_mask = np.logical_not(refractory_mask)
        self.v[update_mask] += ((self.v_leak[update_mask] - self.v[update_mask]) / self.tau_mem +
                                current[update_mask] / self.cap_mem)
        # removed * self.dt -> implicitly measures tau_mem and cap_mem in units of dt
        self.refractory[refractory_mask] -= 1

        self.it += 1
        return spike_mask, self.v, current

    # @profile
    def step_numba_list(self):
        # external current signal
        new_input_cache = []
        external_input = np.zeros(self.num_neurons)
        for ext_inp in self.input_cache:
            external_input += ext_inp[:, 0]
            if ext_inp.shape[1] > 1:
                ext_inp = ext_inp[:, 1:]
                new_input_cache.append(ext_inp)
        self.input_cache = new_input_cache

        # spike mechanism
        spike_mask = np.logical_and(self.v >= self.v_thresh,
                                    np.logical_not(self.refractory > 0))
        self.v[spike_mask] = self.v_reset[spike_mask]
        self.refractory = self.tau_ref * spike_mask

        # delay processing
        if spike_mask.any():
            self.spike_mask_cols[:, :] = False
            self.spike_mask_cols[:, spike_mask] = True
            spike_connect_mask = np.logical_and(self.spike_mask_cols, self.connection_mask)
            delay_cols = self.delay_matrix[spike_connect_mask]
            weight_cols = self.weight_matrix[spike_connect_mask]
            target_cols = self.full_targets[spike_connect_mask]

            sort_indices = np.argsort(delay_cols)
            delays = delay_cols[sort_indices]
            weights = weight_cols[sort_indices]
            targets = target_cols[sort_indices]
            sim_numba_list_put_float(delays, weights, targets, self.spike_ring_buffer, self.current_buffer_pos,
                                     self.max_delay, self.synaptic_kernel, self.tau_exc, self.tau_inh, self.tau_cutoff)
            # for i in range(len(self.spike_ring_buffer)):
            #     print("spike ring buffer entry ", i, " size: ", len(self.spike_ring_buffer[i]))

        spike_input = np.zeros(self.num_neurons)
        current_spikes = self.spike_ring_buffer[self.current_buffer_pos]
        if len(current_spikes) > 0:
            sim_numba_list_get_float(current_spikes, spike_input)
        self.spike_ring_buffer[self.current_buffer_pos] = List([List([0.0, 0.0])])
        self.current_buffer_pos = (self.current_buffer_pos + 1) % (self.max_delay + 1)

        # integration
        current = external_input + spike_input
        refractory_mask = self.refractory > 0
        # dt_mask = self.dt * np.logical_not(refractory_mask)
        update_mask = np.logical_not(refractory_mask)
        # np.logical_and(not_spike_mask, self.refractory <= 0)
        self.v[update_mask] += ((self.v_leak[update_mask] - self.v[update_mask]) / self.tau_mem +
                                current[update_mask] / self.cap_mem)
        # removed * self.dt -> implicitly measures tau_mem and cap_mem in units of dt
        self.refractory[refractory_mask] -= 1

        self.it += 1

        return spike_mask, self.v, current


@numba.jit(nopython=True)
def sim_numba_list_calc_inds_float(spike_mask_cols, connection_inds):
    in_inds = []
    out_inds = []
    for c in spike_mask_cols:
        cinds = np.where(connection_inds[1] == c)[0]
        out_inds.extend(connection_inds[0][cinds])
        in_inds.extend([c] * len(cinds))
    return out_inds, in_inds

@numba.jit(nopython=True)
def sim_numba_trunc_exp(w, tau, tau_cutoff):
    t = np.arange(int(tau * tau_cutoff))
    return w/tau*np.exp(-t / tau)

@numba.jit(nopython=True)
def sim_numba_list_put_float(delays, weights, targets, spike_ring_buffer, current_buffer_pos, max_delay,
                             synaptic_kernel, tau_exc, tau_inh, tau_cutoff):
    d_prev = delays[0]
    first = True
    ring_buffer_pos = 0
    for (d, tar, wei) in zip(delays, targets, weights):
        if wei != 0 and d != 0:
            if d != d_prev or first:
                ring_buffer_pos = (current_buffer_pos + d - 1) % (max_delay + 1)
                first = False
                d_prev = d
            if synaptic_kernel == 0:
                spike_ring_buffer[int(ring_buffer_pos)].append(List([np.int64(tar), np.float64(wei)]))
            elif synaptic_kernel == 1:
                if wei > 0:
                    tau_syn = tau_exc
                elif wei < 0:
                    tau_syn = tau_inh
                else:
                    tau_syn = tau_exc
                for wi, w_syn in enumerate(sim_numba_trunc_exp(wei, tau_syn, tau_cutoff)):
                    spike_ring_buffer[int(ring_buffer_pos + wi) % (max_delay + 1)].append(List([np.int64(tar), np.float64(w_syn)]))


@numba.jit(nopython=True)
def sim_numba_list_get_float(spike_list, spike_input):
    for (tar, wei) in spike_list:
        spike_input[int(tar)] += wei


@numba.jit(nopython=True)
def sim_numba_list_put(delays, weights, targets, spike_ring_buffer, current_buffer_pos, max_delay):
    d_prev = delays[0]
    first = True
    ring_buffer_pos = 0
    for (d, tar, wei) in zip(delays, targets, weights):
        if d != d_prev or first:
            ring_buffer_pos = (current_buffer_pos + d - 1) % (max_delay + 1)
            first = False
            d_prev = d
        spike_ring_buffer[ring_buffer_pos].append(List([np.int64(tar), np.int64(wei)]))


@numba.jit(nopython=True)
def sim_numba_list_get(spike_list, spike_input):
    for (tar, wei) in spike_list:
        spike_input[tar] += wei


# @profile
def pdist_with_pbc(x: np.ndarray, dim_sizes: Union[list, tuple, np.ndarray]):
    """
    Computes pairwise euclidean distances between M N-dimensional vectors with periodic boundaries at the end of each
    dimension
    x           ndarray    M by N ndarray representing M points in N-dimensional space dim_sizes
    ndarray    ndarray of size N indicating the size of each dimension
    """
    x0 = np.broadcast_to(x, (len(x), *x.shape))
    x1 = np.swapaxes(x0, 0, 1)
    dist = cdist_with_pbc(x0, x1, dim_sizes)
    return dist


# @profile
def cdist_with_pbc(x: np.ndarray, y: np.ndarray, dim_sizes: Union[list, tuple, ndarray]):
    """
    Computes euclidean distances between two sets of M N-dimensional vectors with periodic boundaries at the end of
    each dimension. Based on: https://stackoverflow.com/a/11109336
    x            ndarray    M by N ndarray representing
    M points in N-dimensional space y            ndarray    M by N ndarray representing M points in N-dimensional
    space dim_sizes    ndarray    ndarray of size N indicating the size of each dimension
    """
    dim_sizes = np.array(dim_sizes)
    assert x.shape[-1] == len(dim_sizes)
    assert y.shape[-1] == len(dim_sizes)
    delta = np.abs(x - y)
    delta = np.where(delta > 0.5 * dim_sizes, delta - dim_sizes, delta)
    return np.sqrt((delta ** 2).sum(axis=-1))


# @profile
def construct_synapse_matrices_ndim_lattice(sizes: Union[tuple, ndarray],
                                            weight_exc: int, weight_inh: int,
                                            delay_exc: int, delay_inh: int,
                                            range_exc: int, range_inh: int,
                                            range_exc2: int = None, weight_exc2: float = None,
                                            periodic_boundaries: bool = False,
                                            inhibitory_boundaries: bool = False,
                                            precomputed_distances: ndarray = None,
                                            weight_distribution: str = "box",
                                            delay_distribution: str = "box",
                                            excluded_neuron_idxs: list = [],
                                            use_sparse=False) -> Tuple[ndarray, ndarray, ndarray]:
    """
    Constructs the weight and delay matrices of an n-dimensional lattice.
    sizes                   ndarray    sizes of each dimension
    weight_exc/inh          int        weights of excitatory/inhibitory links
    delay_exc/inh           int        delay of excitatory/inhibitory links
    range_exc/inh           int        range of excitatory/inhibitory links
    periodic_boundaries     bool       whether to wrap around the edges
    inhibitory_boundaries   bool       whether to only allow inhibitory feedback from edge neurons
    precomputed_distances   ndarray    enables the use of precomputed distances
    weight_distribution     str        name of distribution function, one of ["box", "quadratic", "sinc", "hardcore"]
    delay_distribution      str        name of distribution function, one of ["box", "linear", "quadratic"]
    """
    num_dims = len(sizes)
    num_points = np.prod(sizes)
    if not use_sparse:
        weight_matrix = np.zeros((num_points, num_points))
        delay_matrix = np.zeros((num_points, num_points))
    else:
        weight_matrix = sparse.lil_matrix((num_points, num_points))
        delay_matrix = sparse.lil_matrix((num_points, num_points))
    # calculate distance matrix between neurons
    if precomputed_distances is None:
        coords = np.meshgrid(*[np.arange(sh) for sh in sizes])
        coords = np.array(coords).reshape(num_dims, num_points).T
        if periodic_boundaries:
            dists = pdist_with_pbc(coords, sizes)
        else:
            # from sklearn.metrics import pairwise_distances
            # dists = pairwise_distances(coords, metric="euclidean")
            # print(sparse.issparse(dists))
            dists = cdist(coords, coords, "euclidean")
    else:
        dists = precomputed_distances

    # calculate masks for excitatory and inhibitory positions
    range_exc_mask = dists <= range_exc
    range_inh_mask = np.logical_and(dists > range_exc, dists <= range_exc + range_inh)
    range_max = range_exc + range_inh # this is updated depending on the weight distribution
    if range_exc2 is not None:
        range_exc2_mask = np.logical_and(dists > range_exc + range_inh, dists <= range_exc + range_inh + range_exc2)
    outside_range_mask = dists > range_exc + range_inh if weight_exc2 is None else dists > range_exc + range_inh + range_exc2

    if weight_distribution == "box":
        # box potential: weight_exc until range_exc, weight_inh until range_exc + range_inh, 0 outside
        weight_matrix[range_exc_mask] = weight_exc
        weight_matrix[range_inh_mask] = weight_inh
        range_max = range_exc + range_inh
    elif weight_distribution == "box2":
        weight_matrix[range_exc_mask] = weight_exc
        weight_matrix[range_inh_mask] = weight_inh
        weight_matrix[range_exc2_mask] = weight_exc2
        range_max = range_exc + range_inh + range_exc2
    elif weight_distribution == "linear":
        # range_inh = (1 - weight_inh / weight_exc) * range_exc
        # weight_matrix[dists <= range_inh] = weight_exc * (1 - dists[dists <= range_inh] / range_exc)
        # range_end = -weight_inh / weight_exc * range_exc + range_inh
        # rs_second = np.logical_and(dists > range_inh, dists <= range_end)
        # weight_matrix[rs_second] = weight_inh + weight_exc / range_exc * (dists[rs_second] - range_inh)
        #range_max = range_end
        range1 = dists <= range_exc
        weight_matrix[range1] = -weight_exc / range_exc * dists[range1] + weight_exc
        range2 = np.logical_and(dists > range_exc, dists <= range_exc + 0.5 * range_inh)
        weight_matrix[range2] = -2 * abs(weight_inh) / range_inh * (dists[range2] - range_exc)
        range3 = np.logical_and(dists > range_exc + 0.5 * range_inh, dists <= range_exc + range_inh)
        weight_matrix[range3] = 2 * abs(weight_inh) / range_inh * (dists[range3] - range_exc - 0.5 * range_inh) - abs(weight_inh)
        range_max = range_exc + range_inh + range_exc2
    elif weight_distribution == "linear2":
        range1 = dists <= range_exc
        weight_matrix[range1] = -weight_exc / range_exc * dists[range1] + weight_exc
        range2 = np.logical_and(dists > range_exc, dists <= range_exc + 0.5 * range_inh)
        weight_matrix[range2] = -2 * abs(weight_inh) / range_inh * (dists[range2] - range_exc)
        range3 = np.logical_and(dists > range_exc + 0.5 * range_inh, dists <= range_exc + range_inh)
        weight_matrix[range3] = 2 * abs(weight_inh) / range_inh * (dists[range3] - range_exc - 0.5 * range_inh) - abs(
            weight_inh)

        range4 = np.logical_and(dists > range_exc + range_inh, dists <= range_exc + range_inh + 0.5 * range_exc2)
        weight_matrix[range4] = 2 * weight_exc2 / range_exc2 * (dists[range4] - range_exc - range_inh)
        range5 = np.logical_and(dists > range_exc + range_inh + 0.5 * range_exc2, dists <= range_exc + range_inh + range_exc2)
        weight_matrix[range5] = -2 * weight_exc2 / range_exc2 * (dists[range5] - range_exc - range_inh - 0.5 * range_exc2) + weight_exc2
        range_max = range_exc + range_inh + range_exc2
    elif weight_distribution == "quadratic":
        # quadratic approximation to box potential
        weight_matrix[range_exc_mask] = -weight_exc * (dists[range_exc_mask] / range_exc) ** 2 + weight_exc
        weight_matrix[range_inh_mask] = (abs(weight_inh) / (range_inh / 2) ** 2) * (
                dists[range_inh_mask] - (range_exc + range_inh / 2)) ** 2 + weight_inh
        range_max = range_exc + range_inh
    elif weight_distribution == "quadratic2":
        weight_matrix[range_exc_mask] = -weight_exc * (dists[range_exc_mask] / range_exc) ** 2 + weight_exc
        #range2 = np.logical_and(rs > r_exc, rs <= r_exc + r_inh)
        weight_matrix[range_inh_mask] = (abs(weight_inh) / (range_inh / 2) ** 2) * ((dists[range_inh_mask] - (range_exc + range_inh / 2))) ** 2 + weight_inh
        range3 = np.logical_and(dists > range_exc + range_inh, dists <= range_exc + range_inh + range_exc2)
        weight_matrix[range3] = -(weight_exc2 / (range_exc2 / 2) ** 2) * ((dists[range3] - (range_exc + range_inh + 0.5 * range_exc2))) ** 2 + weight_exc2
        range_max = range_exc + range_inh + range_exc2
    elif weight_distribution == "sqrt":
        weight_matrix[range_exc_mask] = -weight_exc * (dists[range_exc_mask] / range_exc) ** 0.5 + weight_exc
        range2 = np.logical_and(dists > range_exc, dists <= range_exc + 0.5 * range_inh)
        weight_matrix[range2] = (abs(weight_inh) / (range_inh / 2) ** 0.5) * ((-dists[range2] + (range_exc + range_inh / 2))) ** 0.5 + weight_inh
        range3 = np.logical_and(dists > range_exc + 0.5 * range_inh, dists <= range_exc + range_inh)
        weight_matrix[range3] = (abs(weight_inh) / (range_inh / 2) ** 0.5) * ((dists[range3] - (range_exc + range_inh / 2))) ** 0.5 + weight_inh
        range_max = range_exc + range_inh
    elif weight_distribution == "sqrt2":
        weight_matrix[range_exc_mask] = -weight_exc * (dists[range_exc_mask] / range_exc) ** 0.5 + weight_exc
        range2 = np.logical_and(dists > range_exc, dists <= range_exc + 0.5 * range_inh)
        weight_matrix[range2] = (abs(weight_inh) / (range_inh / 2) ** 0.5) * ((-dists[range2] + (range_exc + range_inh / 2))) ** 0.5 + weight_inh
        range3 = np.logical_and(dists > range_exc + 0.5 * range_inh, dists <= range_exc + range_inh)
        weight_matrix[range3] = (abs(weight_inh) / (range_inh / 2) ** 0.5) * ((dists[range3] - (range_exc + range_inh / 2))) ** 0.5 + weight_inh
        range4 = np.logical_and(dists > range_exc + range_inh, dists <= range_exc + range_inh + 0.5 * range_exc2)
        weight_matrix[range4] = -(weight_exc2 / (range_exc2 / 2) ** 0.5) * ((-dists[range4] + (range_exc + range_inh + range_exc2 * 0.5))) ** 0.5 + weight_exc2
        range5 = np.logical_and(dists > range_exc + range_inh + 0.5 * range_exc2, dists <= range_exc + range_inh + range_exc2)
        weight_matrix[range5] = -(weight_exc2 / (range_exc2 / 2) ** 0.5) * ((dists[range5] - (range_exc + range_inh + range_exc2 * 0.5))) ** 0.5 + weight_exc2
        range_max = range_exc + range_inh + range_exc2
    elif weight_distribution == "sinc":
        param = 3  # this is a free parameter

        # first maximum and minimum of sinc function are used as potential, cutoff at range_exc + range_inh to 0
        def nth_sinc_extremum(n):
            q = (n + 1 / 2) * np.pi
            x = q - q ** (-1) - 2 / 3 * q ** (-3) - 13 / 15 * q ** (-5) - 146 / 105 * q ** (-7)
            return x, np.sinc(x / np.pi)

        x_firstmin, y_firstmin = nth_sinc_extremum(1)
        x_firstmax, y_firstmax = nth_sinc_extremum(2)

        sinc_height = 1 - y_firstmin
        below_firstmax_mask = dists <= (x_firstmax * (range_exc / param))
        np.putmask(weight_matrix, below_firstmax_mask, (weight_exc - weight_inh) / sinc_height * (
                np.sinc(dists / (range_exc / param) / np.pi) - y_firstmin) + weight_inh)
        range_max = range_exc + range_inh
    elif weight_distribution == "hardcore":
        # cutoff to weight_exc at low radii, minimum at weight_inh around range_exc, asymptotically approaches zero
        # for r -> oo
        weight_matrix = np.minimum(abs(4 * weight_inh) * ((range_exc / dists) ** 2 - (range_exc / dists) ** 1),
                                   weight_exc * np.ones_like(dists))
        range_max = np.inf
    elif weight_distribution == "box_gap":
        weight_matrix[range_exc_mask] = weight_exc
        weight_matrix[np.logical_and(dists > range_exc + range_exc2, dists <= range_exc + range_exc2 + range_inh)] = weight_inh
        range_max = range_exc + range_inh + range_exc2

    if inhibitory_boundaries:
        # here we could also choose 'range_inh_mask' instead, this would enlarge the border distance
        num_exc_connections = np.sum(range_exc_mask, axis=0)
        # if ".T" at the end edge neurons can still spike, but only send out inhibitory signals
        # if not edge neurons are not being sent excitatory signals, i.e. they should be dead
        mask_edge_neurons = np.repeat(num_exc_connections < np.max(num_exc_connections),
                                      num_points).reshape(*weight_matrix.shape).T
        # print(mask_edge_neurons.tolist())
        # print(mask_edge_neurons[:, 0].tolist())
        mask_edge_exc_weights = np.logical_and(weight_matrix > 0, mask_edge_neurons)
        weight_matrix[mask_edge_exc_weights] = 0

    # exclude specified neuron indices from network by cutting off all synapses
    if len(excluded_neuron_idxs) > 0:
        weight_matrix[excluded_neuron_idxs, :] = 0.0
        weight_matrix[:, excluded_neuron_idxs] = 0.0

    # create outside mask for delay distribution
    outside_range_mask = dists > range_max

    if delay_distribution == "box":
        delay_matrix[range_exc_mask] = delay_exc
        if range_exc2_mask is not None:
           delay_matrix[range_exc2_mask] = delay_exc  # prevously: delay_exc
        delay_matrix[range_inh_mask] = delay_inh
    elif delay_distribution == "linear":
        delay_matrix = np.around((delay_inh - delay_exc) * dists / range_exc) + 1
        # if weight_distribution != "hardcore":
        #     delay_matrix[outside_range_mask] = 0
    elif delay_distribution == "quadratic":
        delay_matrix = np.around((delay_inh - delay_exc) * dists ** 2 / range_exc**2) + 1
        # if weight_distribution != "hardcore":
        #     delay_matrix[outside_range_mask] = 0
    delay_matrix[outside_range_mask] = 0

    if not use_sparse:
        np.fill_diagonal(delay_matrix, 0.0)
        np.fill_diagonal(weight_matrix, 0.0)
    else:
        delay_matrix = delay_matrix.tocsr()
        delay_matrix.setdiag(0)
        weight_matrix = weight_matrix.tocsr()
        # delay_matrix = delay_matrix.tolil()
        weight_matrix.setdiag(0)
        # weight_matrix = weight_matrix.tolil()

    plotting = False
    if plotting:
        import matplotlib.pyplot as plt
        plt.figure()
        plt.imshow(weight_matrix)
        plt.colorbar()
        plt.savefig("weight_matrix_{}.png".format(weight_distribution))
    return weight_matrix, delay_matrix, dists
