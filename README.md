# iSpiNN

An Interactive Spiking Neural Network Simulator

## Description
This project implements a spiking neural network simulator which is linked to a comprehensive user interface in order to facilitate direct interaction with the network dynamics, e.g. through injecting currents into a region of neurons by clicking there.
Currently the network structure is 2D and weight distributions over distance are based on the Euclidean metric.

## User Interface
Supported features:
- Loading/saving parameter settings
- Saving simulations as mp4 movies (requires ffmpeg)
- Interactively injecting currents of specified size and duration
- Live plotting of various variables and statistics

![](user_interface.png)

## Installation

1. Clone the repository
```
git clone https://gitlab.com/rkla/ispinn.git
cd ispinn
```
2. Setup a new conda environment using the provided YAML file
```
conda env create -f environment.yml
conda activate ispinn
```
3. Run the main file
```
python main.py
```

## Compiling the C++ backend
1. Make sure you have a Cpp compiler installed that is compatible with your pybind11 and xtensor versions.
2. Run `spinn_test.py` which auto-compiles the simulation code using cppimport
```
python spinn_test.py
```


## License
MIT
