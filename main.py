import os
import sys
from pathlib import Path
import threading
import time
import json
import re
from pathlib import Path
from typing import Union, Tuple, List

import PIL.Image
from PIL import Image

import wx
import wx.lib.scrolledpanel as scroll
import wx.propgrid as pg

import numpy as np
from scipy.spatial.distance import cdist
from scipy.special import gamma

import matplotlib as mpl

mpl.use('WXAgg')
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.figure import Figure
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigureCanvas
from matplotlib.backends.backend_wxagg import NavigationToolbar2WxAgg as NavigationToolbar2Wx

from simulation import construct_synapse_matrices_ndim_lattice, LIFSimulation
from utils import FFMPEGVideo

simulation_settings = {
    "simulator": ["enum", {"labels": ["Numpy", "Numpy + Numba", "XTensor"], "values": [0, 1, 2], "value": 2}, {}],
    "dt": [float, {"value": 0.1}, {"min": 1e-10, "max": 1, "step": 0.1}],
    "max_steps": [int, {"value": -1}, {"min": -1, "max": 1e6}],
}

save_settings = {
    "save_settings": [bool, {}, {}],
    "record_spike_movie": [bool, {}, {}],
    "record_potential_movie": [bool, {}, {}],
    "record_current_movie": [bool, {}, {}],
    "file_name": [str, {"value": "my_experiment"}, {}],
    "file_path": ["dir", {"value": os.path.join(Path.home(), "ispinn_data")},
                  {}],
    "fps": [int, {"value": 8}, {"max": 200, "min": 1}],
}

network_widgets = {
    "grid_size_x": [int, {"value": 50}, {"max": 1000, "min": 3}],
    "grid_size_y": [int, {"value": 50}, {"max": 1000, "min": 3}],
    "periodic_boundaries": [bool, {}, {}],
    "inhibitory_boundaries": [bool, {"value": True}, {}]
}

neuron_widgets = {
    "v_thresh": [float, {"value": 1}, {"max": 100, "min": -100, "step": 1}],
    "v_reset": [float, {"value": 0}, {"max": 100, "min": -100, "step": 1}],
    "v_leak": [float, {"value": 0}, {"max": 100, "min": -100, "step": 1}],
    "tau_ref": [int, {"value": 0}, {"max": 100, "min": 0, "step": 1}, "[dt]"],
    "tau_mem": [float, {"value": 1}, {"max": 100, "min": 1e-10, "step": 1}, "[dt]"],
    "cap_mem": [float, {"value": 0.1}, {"max": 100, "min": 1e-10, "step": .1}, "[dt]"],
}

synapse_widgets = {
    "weight_distribution": ["enum",
                            {"labels": ["Box", "Box2", "Quadratic", "Sinc", "Hardcore", "Linear", "Linear2", "Quadratic2", "Sqrt", "Sqrt2", "Box Gap"], "values": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]},
                            {}],
    "weight_exc": [float, {"value": 0.2}, {"max": 100, "min": 0, "step": 0.1}],
    "weight_exc2": [float, {"value": 0}, {"max": 100, "min": 0, "step": 0.1}],
    "weight_inh": [float, {"value": 0.1}, {"max": 100, "min": 0, "step": 0.1}],
    "delay_distribution": ["enum", {"labels": ["Box", "Linear", "Quadratic"], "values": [0, 1, 2]}, {}],
    "delay_exc": [int, {"value": 1}, {"max": 100, "min": 1, "step": 1}, "[dt]"],
    "delay_inh": [int, {"value": 4}, {"max": 100, "min": 1, "step": 1}, "[dt]"],
    "distance_measure": ["enum", {"labels": ["Euclidean"], "values": [0]}, {}],
    "range_exc": [float, {"value": 3.5}, {"max": 100, "min": 1, "step": 0.1}],
    "range_exc2": [float, {"value": 1}, {"max": 100, "min": 0, "step": 0.1}],
    "range_inh": [float, {"value": 3}, {"max": 100, "min": 0, "step": 0.1}],
    "synaptic_kernel": ["enum", {"labels": ["Delta", "Exp"], "values": [0, 1]}, {}],
    "tau_exc": [float, {"value": 1}, {"max": 100, "min": 1e-10, "step": 1}, "[dt]"],
    "tau_inh": [float, {"value": 1}, {"max": 100, "min": 1e-10, "step": 1}, "[dt]"],
    "tau_cutoff": [float, {"value": 2}, {"max": 100, "min": 1e-10, "step": 1}, "[dt]"],
    "exclude_neurons": [str, {"value": ""}, {}],
}

input_widgets = {
    "input_pos": [int, {"value": -1}, {"max": 10000, "min": -1}, ""],
    # "init_input": [bool, {"value": False}, {}],
    "input_period": [int, {"value": -1}, {"max": 10000, "min": -1}, "[dt]"],
    "input_shape": ["enum", {"labels": ["Square", "Circle"], "values": [0, 1, 2]}, {}],
    "input_strength": [float, {"value": 3}, {"max": 100, "min": -100, "step": 1}],
    "input_space": [float, {"value": 4}, {"max": 100, "min": 1, "step": 2}],
    "input_time": [int, {"value": 5}, {"max": 100, "min": 1}, "[dt]"],
    "input_pos2": [int, {"value": -1}, {"max": 10000, "min": -1}, ""],
    "input_period2": [int, {"value": -1}, {"max": 10000, "min": -1}, "[dt]"],
    "input_shape2": ["enum", {"labels": ["Square", "Circle"], "values": [0, 1, 2]}, {}],
    "input_strength2": [float, {"value": 3}, {"max": 100, "min": -100, "step": 1}],
    "input_space2": [float, {"value": 4}, {"max": 100, "min": 1, "step": 2}],
    "input_time2": [int, {"value": 15}, {"max": 100, "min": 1}, "[dt]"]
}

settings_widget_dict = {
    "simulation settings": simulation_settings,
    "network settings": network_widgets,
    "neuron settings": neuron_widgets,
    "synapse settings": synapse_widgets,
    "input settings": input_widgets,
    "save settings": save_settings}

weight_distribution_map = {0: "box", 1: "box2", 2: "quadratic", 3: "sinc", 4: "hardcore", 5: "linear", 6: "linear2", 7: "quadratic2", 8: "sqrt", 9: "sqrt2", 10: "box_gap"}
delay_distribution_map = {0: "box", 1: "linear", 2: "quadratic"}


class NeuronRecord:
    def __init__(self, plot_type="single", neuron_idx=0, neuron_coords=(0, 0)):
        assert plot_type in ["single", "average_x", "average_y", "slice_x", "slice_y", "radial_average"]
        self.plot_type = plot_type
        self.neuron_idx = neuron_idx
        self.neuron_coords = neuron_coords
        self.times = []
        self.data = []

    def add_point(self, time_step, data_point):
        self.times.append(time_step)
        self.data.append(data_point)

    def reset(self):
        self.times = []
        self.data = []


def parse_exclude_neurons(exclude_string: str, grid_size: Tuple[int, int]) -> List[int]:
    """ Parses a string of different syntax elements defining neurons excluded from the synaptic connectivity
    :param grid_size: the grid sizes of the neuron sheet
    :param exclude_string: the string describing excluded neurons; comma-separated list; each expression defines either
    - a single neuron index to be excluded, e.g. '42'
    - a linear range of indices, eg. '42-103'
    - a box of indices by start and end neuron indices, e.g. '42x78'
    :return: list of excluded indices
    """
    num_neurons = grid_size[0] * grid_size[1]
    indices = []
    for element in exclude_string.split(","):
        element = element.strip()
        print(element)
        if re.match("\d+-\d+", element):  # linear range
            from_str, to_str = element.split("-")
            from_idx, to_idx = int(from_str), int(to_str)
            if from_idx < num_neurons and to_idx < num_neurons and from_idx < to_idx:
                indices.extend(list(range(from_idx, to_idx + 1)))
        elif re.match("\d+x\d+", element):  # box
            from_str, to_str = element.split("x")
            from_idx, to_idx = int(from_str), int(to_str)
            if from_idx < num_neurons and to_idx < num_neurons:
                x_coord_from, y_coord_from = from_idx // grid_size[0], from_idx % grid_size[0]
                x_coord_to, y_coord_to = to_idx // grid_size[0], to_idx % grid_size[0]
                if x_coord_from > x_coord_to:
                    x_coord_from, x_coord_to = x_coord_to, x_coord_from
                if y_coord_from > y_coord_to:
                    y_coord_from, y_coord_to = y_coord_to, y_coord_from
                print(x_coord_from, x_coord_to)
                print(y_coord_from, y_coord_to)
                for xi in range(x_coord_from, x_coord_to + 1):
                    for yi in range(y_coord_from, y_coord_to + 1):
                        # neuron_coords[1] * self.frame.grid_size[0] + neuron_coords[0]
                        neuron_idx = xi * grid_size[0] + yi
                        print()
                        indices.append(neuron_idx)
        elif re.match("\d+", element):  # only contains digits, i.e. single index
            index = int(element)
            if index < grid_size[0] * grid_size[1]:
                indices.append(int(element))
    return indices


def normed_angular_integral(data: np.ndarray, origin: Union[tuple, list, np.ndarray],
                            precomputed_dists: np.ndarray = None, r_bin: int = 1):
    """ Integrates the angular part of the given cartesian data array around a the given origin
    data                np.ndarray                  n-dimensional scalar field on a cartesian lattice
    origin              tuple, list, np.ndarray     n-dimensional vector indicating the origin of the coord. system
    precomputed_dists   None, np.ndarray            precomputed distance matrix between lattice points and origin
    r_bin               int                         size of the radial integration bins
    """

    assert (np.array(list(origin)) >= 0).all()
    dim = len(origin)
    if dim == 0:
        assert data.shape == (1,)
        return data[0]
    elif dim >= 1:
        assert len(data.shape) >= 1
        origin = np.array(list(origin))
        coords = np.meshgrid(*[np.arange(sh) for sh in data.shape])
        coords = np.array((coords[0].T, coords[1].T))

        if precomputed_dists is None:
            # compute distance matrix of the coords to origin
            orig_exp = origin.copy()
            for i in range(dim):
                orig_exp = np.expand_dims(orig_exp, axis=1)
            orig_broad = np.broadcast_to(orig_exp, (dim, *data.shape))
            dists = np.linalg.norm(coords - orig_broad, axis=0)
        else:
            assert isinstance(precomputed_dists, np.ndarray), "You need to provide a numpy array containing " \
                                                              "precomputed distances "
            dists = precomputed_dists

        # compute the surface factor of the 'dim' sphere
        surface_factor = 2 * np.pi ** (dim / 2) / gamma(dim / 2)

        # calculate the maximal radius of a sphere fitting into the given cuboid
        orig_partitions = np.array([[data.shape[i] - origin[i], origin[i]] for i in range(dim)])
        max_radius = np.min(orig_partitions)

        # define radial integration intervals
        radii = np.arange(1, max_radius, r_bin)

        # calculate the integral
        radial_activity = np.zeros(len(radii))
        for ir, r in enumerate(radii):
            if ir <= 0:  # innermost shell
                r_band = dists <= r
                r_div = r
            else:  # outer shells
                r_band = np.logical_and(dists <= r, dists > radii[ir - 1])
                # take the current radius to be the average between the adjacent shell radii
                r_div = 0.5 * (r + radii[ir - 1])
            radial_activity[ir] = data[r_band].sum() / r_div ** (dim - 1) / surface_factor
        return radial_activity


def ndarray_to_img(ar, amin=None, amax=None, cmap=None):
    grid_size = ar.shape
    if min is None:
        amin = ar.min()
    if max is None:
        amax = ar.max()
    if cmap is None:
        ar_rgb = np.repeat(ar, 3).reshape(*grid_size, 3)
        ar_rgb_norm = ar_rgb.astype(np.float32) / max(amax, 1)
    else:
        norm = mpl.colors.Normalize(vmin=amin, vmax=amax)
        ar_rgb_norm = cmap(norm(ar), alpha=None)[:, :, :3]
    img_ar = (255 * ar_rgb_norm).astype(np.uint8)

    return img_ar


def display_to_grid(disp_x, disp_y, grid_size_loc, display_size_loc):
    grid_x = int(disp_x * grid_size_loc[0] / display_size_loc[0])
    grid_y = int(disp_y * grid_size_loc[1] / display_size_loc[1])
    return grid_x, grid_y


def grid_to_display(grid_x, grid_y, grid_size_loc, display_size_loc):
    disp_x = grid_x * int(display_size_loc[0] / grid_size_loc[0])
    disp_y = grid_y * int(display_size_loc[1] / grid_size_loc[1])
    return disp_x, disp_y


def calc_input_tensor(shape, grid_size, center_coords, radius, timespan, ndims=2):
    input_tensor = np.zeros((*grid_size, timespan))
    # old_input_box = tuple([slice(center_coords[n] - radius, center_coords[n] + radius, 1)
    #                       for n in range(num_dims)] + [slice(0, timespan, 1)])
    if shape == 0:  # square
        x_high, x_low = min(center_coords[0] + radius, grid_size[0] - 1), max(center_coords[0] - radius, 0)
        y_high, y_low = min(center_coords[1] + radius, grid_size[1] - 1), max(center_coords[1] - radius, 0)
        input_box = tuple([slice(x_low, x_high, 1), slice(y_low, y_high, 1), slice(0, timespan, 1)])
        input_tensor[input_box] = 1
    elif shape == 1:  # circle
        x_high, x_low = min(center_coords[0] + radius + 1, grid_size[0] - 1), max(center_coords[0] - radius, 0)
        y_high, y_low = min(center_coords[1] + radius + 1, grid_size[1] - 1), max(center_coords[1] - radius, 0)
        input_width = x_high - x_low
        input_height = y_high - y_low
        input_box = tuple([slice(x_low, x_high, 1), slice(y_low, y_high, 1), slice(0, timespan, 1)])
        coords = np.meshgrid(*[np.arange(x_low, x_high, 1),
                               np.arange(y_low, y_high, 1)])
        coords_reshaped = np.array(coords).reshape(ndims, -1).T
        input_dist = np.linalg.norm(coords_reshaped - center_coords, axis=1)
        circle_mask = input_dist <= radius
        input_tensor[input_box][circle_mask.reshape(input_width, input_height), :] = 1
    input_tensor = np.swapaxes(input_tensor, 0, 1)
    return input_tensor


class RightClickMenu(wx.Menu):
    var_strings = ["spikes", "potentials", "currents"]

    def __init__(self, frame, position, var_id):
        super(RightClickMenu, self).__init__()

        self.position = position
        self.frame = frame
        self.var_id = var_id

        mmi = wx.MenuItem(self, wx.ID_ANY, 'Stimulate with input2', "stimulate with input2 specs")
        self.Append(mmi)
        self.Bind(wx.EVT_MENU, self.on_input, mmi)

        cmi = wx.MenuItem(self, wx.ID_ANY, f'Record this neurons\'s {self.var_strings[self.var_id]}',
                          'record neuron dynamics')
        self.Append(cmi)
        self.Bind(wx.EVT_MENU, self.on_record, cmi)

        cmi = wx.MenuItem(self, wx.ID_ANY, 'Record this neuron\'s variables', 'record neuron dynamics')
        self.Append(cmi)
        self.Bind(wx.EVT_MENU, self.on_record_all, cmi)

        xmi = wx.MenuItem(self, wx.ID_ANY, 'X average', 'x average dynamics')
        self.Append(xmi)
        self.Bind(wx.EVT_MENU, self.on_average_x, xmi)

        ymi = wx.MenuItem(self, wx.ID_ANY, 'Y average', 'y average dynamics')
        self.Append(ymi)
        self.Bind(wx.EVT_MENU, self.on_average_y, ymi)

        xsi = wx.MenuItem(self, wx.ID_ANY, 'X slice', 'x slice dynamics')
        self.Append(xsi)
        self.Bind(wx.EVT_MENU, self.on_slice_x, xsi)

        ysi = wx.MenuItem(self, wx.ID_ANY, 'Y slice', 'y slice dynamics')
        self.Append(ysi)
        self.Bind(wx.EVT_MENU, self.on_slice_y, ysi)

        rmi = wx.MenuItem(self, wx.ID_ANY, 'Radial average', 'radial average dynamics')
        self.Append(rmi)
        self.Bind(wx.EVT_MENU, self.on_average_radial, rmi)

    def on_input(self, evt):
        if not self.frame.stop:
            mouse_pos = self.position
            grid_click_coords = display_to_grid(*mouse_pos, self.frame.grid_size, self.frame.display_size)
            mouse_input = calc_input_tensor(self.frame.input_shape2, self.frame.grid_size, grid_click_coords,
                                            self.frame.input_space2 // 2,
                                            self.frame.input_time2)
            mouse_input = self.frame.input_strength2 * mouse_input.reshape(-1,
                                                                           self.frame.input_time2)
            # self.frame.simulation_generator.input_cache.append(mouse_input)
            self.frame.simulation_generator.add_input(mouse_input)

    def on_record(self, evt):
        if not self.frame.stop:
            mouse_pos = self.position
            print("mouse pos: ", self.position)
            neuron_coords = display_to_grid(*mouse_pos, self.frame.grid_size, self.frame.display_size)
            print("grid position: ", neuron_coords)
            neuron_idx = neuron_coords[1] * self.frame.grid_size[0] + neuron_coords[0]
            # neuron_pos_0 = neuron_idx % grid_size[0]; neuron_pos_1 = neuron_idx // grid_size[0]
            self.frame.neuron_record[self.var_id] = NeuronRecord("single", neuron_idx, neuron_coords)

    def on_record_all(self, evt):
        if not self.frame.stop:
            mouse_pos = self.position
            print("mouse pos: ", self.position)
            neuron_coords = display_to_grid(*mouse_pos, self.frame.grid_size, self.frame.display_size)
            print("grid position: ", neuron_coords)
            neuron_idx = neuron_coords[1] * self.frame.grid_size[0] + neuron_coords[0]
            for var_id in range(3):
                self.frame.neuron_record[var_id] = NeuronRecord("single", neuron_idx, neuron_coords)

    def on_average_x(self, evt):
        if not self.frame.stop:
            self.frame.neuron_record[self.var_id] = NeuronRecord("average_x")

    def on_average_y(self, evt):
        if not self.frame.stop:
            self.frame.neuron_record[self.var_id] = NeuronRecord("average_y")

    def on_slice_x(self, evt):
        if not self.frame.stop:
            mouse_pos = self.position
            neuron_coords = display_to_grid(*mouse_pos, self.frame.grid_size, self.frame.display_size)
            neuron_idx = neuron_coords[1] * self.frame.grid_size[0] + neuron_coords[0]
            self.frame.neuron_record[self.var_id] = NeuronRecord("slice_x", neuron_idx, neuron_coords)

    def on_slice_y(self, evt):
        if not self.frame.stop:
            mouse_pos = self.position
            neuron_coords = display_to_grid(*mouse_pos, self.frame.grid_size, self.frame.display_size)
            neuron_idx = neuron_coords[1] * self.frame.grid_size[0] + neuron_coords[0]
            self.frame.neuron_record[self.var_id] = NeuronRecord("slice_y", neuron_idx, neuron_coords)

    def on_average_radial(self, evt):
        mouse_pos = self.position
        origin = display_to_grid(*mouse_pos, self.frame.grid_size, self.frame.display_size)
        origin_id = origin[0] + origin[1] * self.frame.grid_size[0]
        self.frame.neuron_record[self.var_id] = NeuronRecord("radial_average", origin_id)


class SimPanel(wx.Panel):
    def __init__(self, parent, frame, var_id, *args):
        wx.Panel.__init__(self, parent, *args)
        self.frame = frame
        self.parent = parent
        self.var_id = var_id
        self.bitmap = None
        self.Bind(wx.EVT_PAINT, self.on_paint)
        self.SetDoubleBuffered(True)
        self.SetCursor(wx.Cursor(wx.CURSOR_CROSS))
        self.Bind(wx.EVT_RIGHT_DOWN, self.on_right_down)
        self.Bind(wx.EVT_MOTION, self.on_move)
        # cdc = wx.ClientDC(self)
        # self.parent.PrepareDC(cdc)
        self.mpl_panel = None

        # selected neuron
        self.neuron_size = None
        self.plot_type = None
        self.selected_neuron_pos = None
        self.selected_color = 255, 0, 0, 0
        self.selected_size = 4

    def on_right_down(self, evt):
        self.PopupMenu(RightClickMenu(self.frame, evt.GetPosition(), self.var_id), evt.GetPosition())

    def on_move(self, evt):
        if not self.frame.stop:
            grid_pos = display_to_grid(*evt.GetPosition(), self.frame.grid_size, self.frame.display_size)
            self.frame.m_statusBar1.SetStatusText("Neuron position: {}".format(grid_pos), 1)

    def on_paint(self, evt):
        if self.bitmap is not None:
            dc = wx.BufferedPaintDC(self)
            dc.Clear()
            dc.DrawBitmap(self.bitmap, (0, 0))
            if self.selected_neuron_pos is not None:
                self.neuron_size = int(self.frame.display_size[0] / self.frame.grid_size[0])
                dc.SetPen(wx.TRANSPARENT_PEN)
                if self.plot_type == "single":
                    brush = wx.Brush(self.selected_color, style=wx.BRUSHSTYLE_SOLID)
                    dc.SetBrush(brush)
                    dc.DrawRectangle(self.selected_neuron_pos[0],
                                     self.selected_neuron_pos[1],
                                     self.neuron_size, self.neuron_size)
                elif self.plot_type == "slice_x":
                    brush = wx.Brush(self.selected_color, style=wx.BRUSHSTYLE_BDIAGONAL_HATCH)
                    dc.SetBrush(brush)
                    dc.DrawRectangle(0,
                                     self.selected_neuron_pos[1],
                                     self.frame.display_size[0], self.neuron_size)
                elif self.plot_type == "slice_y":
                    brush = wx.Brush(self.selected_color, style=wx.BRUSHSTYLE_BDIAGONAL_HATCH)
                    dc.SetBrush(brush)
                    dc.DrawRectangle(self.selected_neuron_pos[0],
                                     0,
                                     self.neuron_size, self.frame.display_size[1])


class MPLCanvasPanel(wx.Panel):
    def __init__(self, parent, xlabel="", ylabel="", *args):
        wx.Panel.__init__(self, parent, *args)
        # self.SetMaxSize((parent.GetSize()[0], -1))

        # create figure and widgets
        self.figure = Figure(figsize=(3.8, 2.6))
        self.axes = self.figure.add_subplot(111)

        self.canvas = FigureCanvas(self, -1, self.figure)
        self.toolbar = NavigationToolbar2Wx(self.canvas)
        self.toolbar.Realize()

        # define the layout
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(self.canvas, 1, wx.LEFT | wx.TOP | wx.GROW)
        sizer.Add(self.toolbar, 0, wx.LEFT | wx.EXPAND)
        self.SetSizer(sizer)
        self.Fit()

        # plot attributes
        self.xlabel = xlabel
        self.ylabel = ylabel
        self.xlims = None
        self.ylims = None
        self.axes.set_xlabel(self.xlabel)
        self.axes.set_ylabel(self.ylabel)

        # setup line
        # t = np.arange(0.0, 3.0, 0.01)
        # s = np.sin(2 * np.pi * t)
        # self.lines = self.axes.plot([], [])
        # self.figure.tight_layout()
        # self.canvas.draw()
        # self.figure.show(block=False)
        # self.background = self.canvas.copy_from_bbox(self.axes.bbox)
        self.reset()

    def reset(self):
        self.axes.clear()
        self.lines = self.axes.plot([], [])
        self.axes.set_xlabel(self.xlabel)
        self.axes.set_ylabel(self.ylabel)
        self.axes.relim()
        # self.axes.autoscale_view()
        self.figure.tight_layout()
        self.canvas.draw()

    def update(self, xdata, ydata, xlabel=None, ylabel=None, xlims=None, ylims=None):
        if xlabel is not None:
            self.axes.set_xlabel(xlabel)
        if ylabel is not None:
            self.axes.set_ylabel(ylabel)
        if xlims is not None:
            if xlims[0] != xlims[1] and self.xlims is None:
                self.xlims = xlims
                self.axes.set_xlim(xlims)
        if ylims is not None:
            if ylims[0] != ylims[1] and self.ylims is None:
                self.ylims = ylims
                self.axes.set_ylim(ylims)
        # self.canvas.restore_region(self.background)
        self.lines.pop(0).remove()
        self.lines = self.axes.plot(xdata, ydata, c="k")
        self.axes.relim()
        # self.axes.autoscale_view()
        # self.line.set_data(xdata, ydata)
        # self.axes.draw_artist(self.axes.patch)
        # self.axes.draw_artist(self.line)
        self.figure.canvas.draw_idle()
        # self.canvas.update()
        # self.canvas.blit(self.axes.bbox)
        # self.canvas.flush_events()
        # self.Refresh()


class INNSFrame(wx.Frame):

    def __init__(self, parent):
        wx.Frame.__init__(self, parent, id=wx.ID_ANY, title="Interactive Spiking Neural Network Simulation",
                          pos=wx.DefaultPosition,
                          size=wx.Size(1200, 900), style=wx.DEFAULT_FRAME_STYLE | wx.TAB_TRAVERSAL)

        bSizer1 = wx.BoxSizer(wx.HORIZONTAL)

        # Status Bar
        self.m_statusBar1 = self.CreateStatusBar()
        self.m_statusBar1.SetFieldsCount(3)
        self.m_statusBar1.SetStatusWidths([200, 200, 200])
        self.m_statusBar1.SetStatusText("Ready", 0)
        self.m_statusBar1.SetStatusText("Neuron position: ", 1)
        self.m_statusBar1.SetStatusText("Time step: ", 2)

        # Panel 1
        self.m_panel1 = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        bSizer2 = wx.BoxSizer(wx.VERTICAL)
        self.m_propertyGrid1 = pg.PropertyGrid(self.m_panel1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                               wx.propgrid.PG_DEFAULT_STYLE)
        self.m_propertyGrid1.RegisterAdditionalEditors()
        self.settings_properties = {}

        for group_name, widgets_dict in settings_widget_dict.items():
            self.m_propertyGrid1.Append(pg.PropertyCategory(group_name))
            for setting_name, setting_widget in widgets_dict.items():
                if setting_widget[0] == bool:
                    prop_class = pg.BoolProperty
                    widget_prop = prop_class(setting_name, name=setting_name)
                elif setting_widget[0] == int:
                    prop_class = pg.IntProperty
                    unit = "" if len(setting_widget) < 4 else " " + setting_widget[3]
                    widget_prop = prop_class(setting_name + unit, name=setting_name, **setting_widget[1])
                    widget_prop.SetEditor("SpinCtrl")
                    widget_prop.SetAttribute(wx.propgrid.PG_ATTR_SPINCTRL_STEP, 1)
                    widget_prop.SetAttribute(wx.propgrid.PG_ATTR_SPINCTRL_MOTION, 1)
                    widget_prop.SetAttribute(wx.propgrid.PG_ATTR_MIN, setting_widget[2]["min"])
                    widget_prop.SetAttribute(wx.propgrid.PG_ATTR_MAX, setting_widget[2]["max"])
                elif setting_widget[0] == "dir":
                    widget_prop = pg.DirProperty(setting_name, name=setting_name, **setting_widget[1])
                elif setting_widget[0] == float:
                    unit = "" if len(setting_widget) < 4 else " " + setting_widget[3]
                    widget_prop = pg.FloatProperty(setting_name + unit, name=setting_name, **setting_widget[1])
                    widget_prop.SetEditor("SpinCtrl")
                    widget_prop.SetAttribute(wx.propgrid.PG_ATTR_SPINCTRL_STEP, setting_widget[2]["step"])
                    widget_prop.SetAttribute(wx.propgrid.PG_ATTR_SPINCTRL_MOTION, 1)
                    widget_prop.SetAttribute(wx.propgrid.PG_ATTR_MIN, setting_widget[2]["min"])
                    widget_prop.SetAttribute(wx.propgrid.PG_ATTR_MAX, setting_widget[2]["max"])
                elif setting_widget[0] == "enum":
                    widget_prop = pg.EnumProperty(setting_name, name=setting_name, **setting_widget[1])
                else:
                    widget_prop = pg.StringProperty(setting_name, name=setting_name, **setting_widget[1])
                self.m_propertyGrid1.Append(widget_prop)
                self.settings_properties[setting_name] = widget_prop
        bSizer2.Add(self.m_propertyGrid1, 1, wx.ALL | wx.EXPAND, 5)

        bSizer4 = wx.BoxSizer(wx.HORIZONTAL)

        self.m_button1 = wx.Button(self.m_panel1, wx.ID_ANY, u"Run", wx.DefaultPosition, wx.DefaultSize, 0)
        bSizer4.Add(self.m_button1, 0, wx.ALL, 5)

        # self.m_button2 = wx.Button(self.m_panel1, wx.ID_ANY, u"Stop", wx.DefaultPosition, wx.DefaultSize, 0)
        # bSizer4.Add(self.m_button2, 0, wx.ALL, 5)

        self.m_button_pause = wx.Button(self.m_panel1, wx.ID_ANY, u"Pause", wx.DefaultPosition, wx.DefaultSize, 0)
        bSizer4.Add(self.m_button_pause, 0, wx.ALL, 5)

        self.m_button_step = wx.Button(self.m_panel1, wx.ID_ANY, u"Step", wx.DefaultPosition, wx.DefaultSize, 0)
        bSizer4.Add(self.m_button_step, 0, wx.ALL, 5)

        bSizer2.Add(bSizer4, 0, wx.EXPAND, 5)

        self.m_panel1.SetSizer(bSizer2)
        self.m_panel1.Layout()
        bSizer2.Fit(self.m_panel1)
        bSizer1.Add(self.m_panel1, 1, wx.EXPAND | wx.ALL, 5)

        # Panel 2
        self.m_panel2 = scroll.ScrolledPanel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_panel2.SetupScrolling()
        bSizer3 = wx.BoxSizer(wx.VERTICAL)
        self.m_staticText1 = wx.StaticText(self.m_panel2, wx.ID_ANY, u"Time step: ", wx.DefaultPosition, wx.DefaultSize,
                                           0)
        self.m_staticText1.Wrap(-1)
        bSizer3.Add(self.m_staticText1, 0, wx.ALL, 5)

        # self.box1 = wx.StaticBox(self, -1, u"Spikes")
        self.m_staticText2 = wx.StaticText(self.m_panel2, wx.ID_ANY, u"Spikes", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText2.Wrap(-1)
        bSizer3.Add(self.m_staticText2, 0, wx.ALL, 5)

        self.bitmap_size = tuple([int(self.GetSize()[1]) // 3] * 2)
        self.m_bitmap1 = SimPanel(self.m_panel2, self, 0, wx.ID_ANY, wx.DefaultPosition, self.bitmap_size,
                                  wx.TAB_TRAVERSAL)
        bSizer3.Add(self.m_bitmap1, 1, wx.ALIGN_LEFT | wx.ALL, 5)

        self.m_staticText3 = wx.StaticText(self.m_panel2, wx.ID_ANY, u"Potentials", wx.DefaultPosition, wx.DefaultSize,
                                           0)
        self.m_staticText3.Wrap(-1)
        bSizer3.Add(self.m_staticText3, 0, wx.ALL, 5)

        self.m_bitmap2 = SimPanel(self.m_panel2, self, 1, wx.ID_ANY, wx.DefaultPosition, self.bitmap_size,
                                  wx.TAB_TRAVERSAL)
        bSizer3.Add(self.m_bitmap2, 1, wx.ALL | wx.ALIGN_LEFT, 5)

        self.m_staticText4 = wx.StaticText(self.m_panel2, wx.ID_ANY, u"Currents", wx.DefaultPosition, wx.DefaultSize, 0)
        bSizer3.Add(self.m_staticText4, 0, wx.ALL, 5)

        self.m_bitmap3 = SimPanel(self.m_panel2, self, 2, wx.ID_ANY, wx.DefaultPosition, self.bitmap_size,
                                  wx.TAB_TRAVERSAL)
        bSizer3.Add(self.m_bitmap3, 1, wx.ALL | wx.ALIGN_LEFT, 5)

        self.m_panel2.SetSizer(bSizer3)
        self.m_panel2.Layout()
        bSizer3.Fit(self.m_panel2)
        bSizer1.Add(self.m_panel2, 1, wx.EXPAND | wx.ALL, 5)

        # Panel 3 #wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_panel3 = scroll.ScrolledPanel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_panel3.SetupScrolling()
        self.bSizer9 = wx.BoxSizer(wx.VERTICAL)

        self.m_mplpanel1 = MPLCanvasPanel(self.m_panel3, "time step", "spike", wx.ID_ANY, wx.DefaultPosition,
                                          wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.bSizer9.Add(self.m_mplpanel1, 1, wx.EXPAND | wx.ALL, 5)

        self.m_mplpanel2 = MPLCanvasPanel(self.m_panel3, "time step", "membrane potential", wx.ID_ANY,
                                          wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.bSizer9.Add(self.m_mplpanel2, 1, wx.EXPAND | wx.ALL, 5)

        self.m_mplpanel3 = MPLCanvasPanel(self.m_panel3, "time step", "currents", wx.ID_ANY, wx.DefaultPosition,
                                          wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.bSizer9.Add(self.m_mplpanel3, 1, wx.EXPAND | wx.ALL, 5)

        self.m_panel3.SetSizer(self.bSizer9)
        self.m_panel3.Layout()
        # self.m_panel3.SetAutoLayout(1)
        self.bSizer9.Fit(self.m_panel3)
        bSizer1.Add(self.m_panel3, 1, wx.EXPAND | wx.ALL, 5)

        self.SetSizer(bSizer1)
        self.Layout()

        self.Centre(wx.BOTH)
        self.m_propertyGrid1.SetSplitterLeft()

        self.m_menubar1 = wx.MenuBar()
        self.m_menu1 = wx.Menu()
        self.m_menuItem1 = self.m_menu1.Append(wx.ID_EXIT, "&Quit\tCtrl+Q", "Quit application")
        self.m_menuItem2 = self.m_menu1.Append(-1, "&Load\tCtrl+L", "Load settings")
        self.m_menubar1.Append(self.m_menu1, "&File")
        self.SetMenuBar(self.m_menubar1)

        self.Centre(wx.BOTH)

        # Connect Events
        self.Bind(wx.EVT_SIZE, self.on_size)
        self.Bind(wx.EVT_MENU, self.on_quit, self.m_menuItem1)
        self.Bind(wx.EVT_MENU, self.on_load_settings, self.m_menuItem2)

        # event handling
        self.m_button1.Bind(wx.EVT_BUTTON, self.on_run)
        # self.m_button2.Bind(wx.EVT_BUTTON, self.on_stop)
        self.m_button_pause.Bind(wx.EVT_BUTTON, self.on_pause)
        self.m_button_step.Bind(wx.EVT_BUTTON, self.on_step)

        # simulation attributes
        self.stop = True
        self.pause = False
        self.step = False
        self.simulation_thread = None

        self.neuron_record = [None, None, None]
        self.grid_size = 50, 50

    def on_load_settings(self, evt):
        # otherwise ask the user what new file to open
        settings_dict = None
        with wx.FileDialog(self, "Open settings file", wildcard="JSON files (*.json)|*.json",
                           style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST) as fileDialog:

            if fileDialog.ShowModal() == wx.ID_CANCEL:
                return  # the user changed their mind

            # Proceed loading the file chosen by the user
            pathname = fileDialog.GetPath()
            try:
                with open(pathname, 'r') as file:
                    settings_dict = json.load(file)
            except IOError:
                print("Cannot open file '%s'." % newfile)

        for group_name, widgets_dict in settings_widget_dict.items():
            for setting_name, setting_widget in widgets_dict.items():
                try:
                    string_setting = settings_dict[setting_name]
                    setting = string_setting  # parse_setting(string_setting)
                    self.settings_properties[setting_name].SetValue(setting)
                except KeyError:
                    pass

    def on_quit(self, evt):
        self.Close()

    def on_size(self, evt):
        self.Layout()

    def on_run(self, event):
        # ensure simulation is not running
        # self.on_stop(None)
        if self.stop:
            # read current settings
            settings = {}
            for prop_name, prop in self.settings_properties.items():
                settings[prop_name] = self.m_propertyGrid1.GetPropertyByName(prop_name).GetValue()

            self.stop = False
            self.simulation_thread = threading.Thread(target=self.run_simulation,
                                                      args=(settings, lambda: self.pause, lambda: self.stop,
                                                            lambda: self.step))
            self.simulation_thread.setDaemon(True)
            self.simulation_thread.start()
            self.m_statusBar1.SetStatusText("Running")
            self.m_button1.SetLabel(u"Stop")
        else:
            if self.simulation_thread is not None:
                self.stop = True
                # while self.simulation_thread.is_alive():
                time.sleep(1)
                # self.simulation_thread.join()
                self.simulation_thread = None
                self.m_statusBar1.SetStatusText("Stopped")

                # reset pause button
                self.pause = False
                self.m_button_pause.SetLabel(u"Pause")
                self.m_button1.SetLabel(u"Run")

    def on_stop(self, event):
        if self.simulation_thread is not None:
            self.stop = True
            # while self.simulation_thread.is_alive():
            time.sleep(1)
            # self.simulation_thread.join()
            self.simulation_thread = None
            self.m_statusBar1.SetStatusText("Stopped")

            # reset pause button
            self.pause = False
            self.m_button_pause.SetLabel(u"Pause")

    def on_step(self, evt):
        if self.pause:
            self.step = True

    def on_pause(self, evt):
        if not self.stop and not self.pause:
            self.pause = True
            self.m_button_pause.SetLabel(u"Continue")
            self.m_statusBar1.SetStatusText("Paused")
        elif not self.stop and self.pause:
            self.pause = False
            self.m_button_pause.SetLabel(u"Pause")
            self.m_statusBar1.SetStatusText("Running")

    def __del__(self):
        pass

    def run_simulation(self, settings, pause_flag, stop_flag, step_flag):
        # reset MPL planes and neuron records
        # self.neuron_record = [None, None, None]
        for nr in self.neuron_record:
            if nr is not None:
                nr.reset()

        self.m_mplpanel1.reset()
        self.m_mplpanel2.reset()
        self.m_mplpanel3.reset()

        # parse settings
        parameter_names = ["dt", "grid_size_x", "grid_size_y", "num_timesteps", "v_thresh", "v_reset", "v_leak",
                           "tau_ref",
                           "tau_mem", "cap_mem", "synaptic_kernel", "tau_exc", "tau_inh", "tau_cutoff",
                           "weight_distribution", "delay_distribution",
                           "input_period", "input_shape", "input_strength", "input_pos", "input_space", "input_time",
                           "input_period2", "input_shape2", "input_strength2", "input_pos2", "input_space2",
                           "input_time2",
                           "weight_exc", "weight_exc2", "weight_inh", "delay_exc", "delay_inh", "range_exc",
                           "range_exc2",
                           "range_inh", "periodic_boundaries", "inhibitory_boundaries"]
        parameter_values = [0.1,  # time step size
                            50,
                            50,
                            0,  # num_timesteps
                            1,  # v_thresh
                            0,  # v_reset
                            0,  # v_leak
                            1,  # tau_ref
                            1,  # tau_mem,
                            1,  # cap_mem
                            0,  # synaptic_kernel
                            1,  # tau_exc
                            1,  # tau_inh
                            2,  # tau_cutoff
                            -1,  # input period
                            0,  # input shape, 0=square, 1=circle, 2=custom
                            1,  # input_strength
                            (25, 25),  # input_pos
                            8,  # input_space
                            4,  # input_time
                            -1,  # input_period2
                            0,  # input shape2, 0=square, 1=circle, 2=custom
                            0,  # input_strength2
                            (0, 0),  # input_pos2
                            8,  # input_space2
                            8,  # input_time2
                            0,  # weight_distribution, 0=box, 1=quadratic, 2=sinc, 3=hardcore
                            0,  # delay_distribution, 0=box, 1=linear, 2=quadrataic
                            0.1,  # weight_exc
                            None,  # weight_exc2
                            0.1,  # weight_inh
                            1,  # delay_exc
                            4,  # delay_inh
                            3,  # range_exc
                            None,  # range_exc2
                            3,  # range_inh
                            False,  # pbc?
                            False,  # inhibitory boundaries?
                            ]
        for key, value in settings.items():
            if key in parameter_names:
                parameter_values[parameter_names.index(key)] = value
        param_dict = dict(zip(parameter_names, parameter_values))
        param_dict["weight_inh"] = -param_dict["weight_inh"]
        param_dict["num_timesteps"] = param_dict["input_time"]
        print(param_dict)

        if param_dict["grid_size_x"] < param_dict["grid_size_y"]:
            grid_size_x = param_dict["grid_size_y"]
            grid_size_y = param_dict["grid_size_x"]
        else:
            grid_size_x = param_dict["grid_size_x"]
            grid_size_y = param_dict["grid_size_y"]
        self.grid_size = grid_size_x, grid_size_y
        display_size_y = min(self.m_bitmap1.GetSize())
        neuron_size = display_size_y // grid_size_y
        display_size_y = neuron_size * grid_size_y
        display_size_x = neuron_size * grid_size_x  # int(grid_size_x / grid_size_y * display_size_y)
        self.display_size = display_size_x, display_size_y
        print("display_size", self.display_size)
        print("grid_size", self.grid_size)
        self.m_bitmap1.SetSize(self.display_size)
        self.m_bitmap2.SetSize(self.display_size)
        self.m_bitmap3.SetSize(self.display_size)
        self.m_bitmap1.SetMinSize(self.display_size)
        self.m_bitmap2.SetMinSize(self.display_size)
        self.m_bitmap3.SetMinSize(self.display_size)
        # self.m_panel3.Layout()
        # self.m_panel3.SetSizerAndFit(self.bSizer9)
        # self.m_panel3.Refresh()
        # self.m_panel3.FitInside()
        self.Layout()

        self.num_neurons = int(np.prod(self.grid_size))
        num_dims = len(self.grid_size)
        self.dt = param_dict["dt"]
        num_time_steps = param_dict["num_timesteps"]
        self.v_thresh1 = param_dict["v_thresh"]
        self.v_reset1 = param_dict["v_reset"]
        self.v_leak1 = param_dict["v_leak"]
        self.tau_ref = param_dict["tau_ref"]
        self.tau_mem = param_dict["tau_mem"]
        self.cap_mem = param_dict["cap_mem"]
        self.synaptic_kernel = param_dict["synaptic_kernel"]
        self.tau_exc = param_dict["tau_exc"]
        self.tau_inh = param_dict["tau_inh"]
        self.tau_cutoff = param_dict["tau_cutoff"]
        self.weight_distribution = param_dict["weight_distribution"]
        self.delay_distribution = param_dict["delay_distribution"]
        self.input_period = param_dict["input_period"]
        self.input_strength = param_dict["input_strength"]
        self.input_shape = param_dict["input_shape"]
        # self.input_pos = param_dict["input_pos"]
        if param_dict["input_pos"] == -1:
            self.input_pos = tuple(self.grid_size[d] // 2 for d in range(len(self.grid_size)))
        elif param_dict["input_pos"] >= 0:
            neuron_pos0 = param_dict["input_pos"] % self.grid_size[0]
            neuron_pos1 = param_dict["input_pos"] // self.grid_size[0]
            self.input_pos = (neuron_pos0, neuron_pos1)
        self.input_space = int(param_dict["input_space"])
        self.input_time = param_dict["input_time"]
        self.input_period2 = param_dict["input_period2"]
        self.input_strength2 = param_dict["input_strength2"]
        self.input_shape2 = param_dict["input_shape2"]
        # self.input_pos2 = param_dict["input_pos2"]
        if param_dict["input_pos2"] == -1:
            self.input_pos2 = tuple(self.grid_size[d] // 2 for d in range(len(self.grid_size)))
        elif param_dict["input_pos2"] >= 0:
            neuron_pos0 = param_dict["input_pos2"] % self.grid_size[0]
            neuron_pos1 = param_dict["input_pos2"] // self.grid_size[0]
            self.input_pos2 = (neuron_pos0, neuron_pos1)
        self.input_space2 = int(param_dict["input_space2"])
        self.input_time2 = param_dict["input_time2"]
        self.J_exc = param_dict["weight_exc"]
        self.J_exc2 = param_dict["weight_exc2"]
        self.J_inh = param_dict["weight_inh"]
        self.delay_exc = param_dict["delay_exc"]
        self.delay_inh = param_dict["delay_inh"]
        self.r_exc = param_dict["range_exc"]
        self.r_exc2 = param_dict["range_exc2"]
        self.r_inh = param_dict["range_inh"]
        self.periodic_boundaries = param_dict["periodic_boundaries"]
        self.inhibitory_boundaries = param_dict["inhibitory_boundaries"]
        # construct external inputs
        ext_input = calc_input_tensor(self.input_shape, self.grid_size, np.array(self.input_pos), self.input_space // 2,
                                      self.input_time)
        ext_input = self.input_strength * ext_input.reshape(-1, self.input_time)

        ext_input2 = calc_input_tensor(self.input_shape2, self.grid_size, np.array(self.input_pos2),
                                       self.input_space2 // 2,
                                       self.input_time2)
        ext_input2 = self.input_strength2 * ext_input2.reshape(-1, self.input_time2)

        # construct synaptic weight and delay matrices
        excluded_neuron_idxs = parse_exclude_neurons(settings["exclude_neurons"], self.grid_size)
        weight_matrix, delay_matrix, distances = construct_synapse_matrices_ndim_lattice(self.grid_size, self.J_exc,
                                                                                         self.J_inh,
                                                                                         self.delay_exc, self.delay_inh,
                                                                                         self.r_exc, self.r_inh,
                                                                                         self.r_exc2, self.J_exc2,
                                                                                         self.periodic_boundaries,
                                                                                         self.inhibitory_boundaries,
                                                                                         None,  # no precomputed dists
                                                                                         weight_distribution_map[
                                                                                             self.weight_distribution],
                                                                                         delay_distribution_map[
                                                                                             self.delay_distribution],
                                                                                         excluded_neuron_idxs,
                                                                                         )

        # run the simulation
        cls = None
        # if settings["dt"] == 1:
        #    cls = DiscreteLIFSimulation
        # elif settings["dt"] < 1:
        step_kwargs = {}
        if settings["simulator"] == 0:
            cls = LIFSimulation
            step_kwargs = {"use_numba": False}
        elif settings["simulator"] == 1:
            cls = LIFSimulation
            step_kwargs = {"use_numba": True}
        elif settings["simulator"] == 2:
            from spinn import LIFSimulation as LIFSimulation_CPP
            cls = LIFSimulation_CPP
            step_kwargs = {"debug": False}
        self.simulation_generator = cls(self.num_neurons, self.dt,
                                        weight_matrix.astype(np.float32), delay_matrix.astype(np.int32),
                                        self.v_reset1,  # * np.ones(self.num_neurons),
                                        self.v_thresh1,  # * np.ones(self.num_neurons),
                                        self.v_leak1,  # * np.ones(self.num_neurons),
                                        None,
                                        None,  # ext_input if settings["init_input"] else None,
                                        self.tau_ref,
                                        self.tau_mem,
                                        self.cap_mem,
                                        self.synaptic_kernel,
                                        self.tau_exc,
                                        self.tau_inh,
                                        self.tau_cutoff)

        data_labels = ["spike", "potential", "current"]
        save_anims = np.array([settings["record_{}_movie".format(data_label)] for data_label in data_labels])
        save_parameters = settings["save_settings"]

        simulation_name = settings["file_name"]

        ffmpegs = []
        save_path = None
        anim_paths = []
        if save_anims.any() or save_parameters:
            if settings["file_path"] == "":
                save_folder = os.path.join(Path.home(), "ispinn_data")
            else:
                save_folder = settings["file_path"]
            Path(save_folder).mkdir(parents=True, exist_ok=True)
            save_path = os.path.join(save_folder, simulation_name)

            if save_anims.any():
                for data_label in data_labels:
                    movie_name = simulation_name + "_" + data_label
                    anim_paths.append(os.path.join(save_folder, movie_name))
                    ffmpegs.append(FFMPEGVideo(movie_name))

        if save_parameters:
            with open(save_path + ".json", "w") as f:
                print("save json")
                json.dump(settings, f)

        # self.simulation_generator.neuron_record = [None, None, None]

        # parse widgets
        def on_left_up(evt):
            mouse_pos = evt.GetPosition()
            grid_click_coords = display_to_grid(*mouse_pos, self.grid_size, self.display_size)
            mouse_input = calc_input_tensor(self.input_shape, self.grid_size, grid_click_coords, self.input_space // 2,
                                            self.input_time)
            # mouse_input = np.swapaxes(mouse_input, 0, 1)
            # print(np.where(mouse_input[:, :, 0] != 0))
            mouse_input = self.input_strength * mouse_input.reshape(-1, self.input_time)
            # print(np.where(mouse_input[:, 0] != 0))
            # print("input container: ",type(self.simulation_generator.input_cache))
            # self.simulation_generator.input_cache.append(mouse_input)
            # print("mouse input dtype: ", mouse_input.dtype)
            # print("mouse input shape: ",mouse_input.shape)
            # print("mouse input flags: ",mouse_input.flags)
            self.simulation_generator.add_input(mouse_input)

        time_text = self.m_staticText1
        state_widgets = [self.m_bitmap1, self.m_bitmap2, self.m_bitmap3]
        mpl_widgets = [self.m_mplpanel1, self.m_mplpanel2, self.m_mplpanel3]
        for widget in state_widgets:
            widget.Bind(wx.EVT_LEFT_UP, on_left_up)

        # initial min/max values for potentials and currents
        cur_min, cur_max = 0, 0
        pot_min, pot_max = 0, 0

        step_times = []
        sim_t = 0
        time_text.SetLabel("Time step = {}".format(sim_t))
        self.m_statusBar1.SetStatusText("Time step = {}".format(sim_t), 2)
        assert isinstance(settings["max_steps"], int)
        max_sim_steps = 1e8 if settings["max_steps"] < 0 else settings["max_steps"]

        # self.stop = False
        while sim_t < max_sim_steps:
            time.sleep(0.0005)
            if stop_flag():
                break
            if pause_flag():
                if not step_flag():
                    continue
                else:
                    self.step = False

            t0 = time.time()
            current_vars = current_spikes, current_pots, current_currents = \
                self.simulation_generator.step(**step_kwargs)
            step_times.append(time.time() - t0)

            # if sim_t < ext_input.shape[1]:
            #     assert (current_currents == ext_input[:, sim_t]).all()
            #     assert (current_currents.reshape(self.grid_size[0], self.grid_size[1])
            #     == ext_input[:, sim_t].reshape(*self.grid_size)).all()
            #     import matplotlib.pyplot as plt
            #     plt.imshow(current_currents.reshape(self.grid_size[1], self.grid_size[0]))
            #     plt.savefig("current_img.png")
            #     print(current_currents.sum())

            # 2d grids
            spike_array_2d = current_spikes.reshape(self.grid_size[1], self.grid_size[0])
            pot_array_2d = current_pots.reshape(self.grid_size[1], self.grid_size[0])
            current_array_2d = current_currents.reshape(self.grid_size[1], self.grid_size[0])
            vars_2d = [spike_array_2d, pot_array_2d, current_array_2d]

            # track neuron variables
            if len(self.neuron_record) > 0:
                for iv, neuron_record in enumerate(self.neuron_record):
                    if neuron_record is not None:
                        if neuron_record.neuron_idx >= self.grid_size[0] * self.grid_size[1]:
                            neuron_record.neuron_idx = self.grid_size[0] * self.grid_size[1] - 1
                            neuron_record.neuron_coords = (self.grid_size[0] - 1, self.grid_size[1] - 1)
                        # update neuron record
                        if neuron_record.plot_type == "single":
                            neuron_record.add_point(sim_t, current_vars[iv][neuron_record.neuron_idx])
                            mpl_widgets[iv].update(neuron_record.times, neuron_record.data, xlabel="time")
                        elif neuron_record.plot_type == "average_x":
                            neuron_record.times = np.arange(self.grid_size[0])
                            neuron_record.data = vars_2d[iv].mean(axis=0)
                            mpl_widgets[iv].update(neuron_record.times, neuron_record.data, xlabel="x coord")
                        elif neuron_record.plot_type == "average_y":
                            neuron_record.times = np.arange(self.grid_size[1])
                            neuron_record.data = vars_2d[iv].mean(axis=1)
                            mpl_widgets[iv].update(neuron_record.times, neuron_record.data, xlabel="y coord")
                        elif neuron_record.plot_type == "slice_x":
                            neuron_record.times = np.arange(self.grid_size[0])
                            neuron_record.data = vars_2d[iv][neuron_record.neuron_coords[1], :]
                            ylims = neuron_record.data.min(), neuron_record.data.max()
                            mpl_widgets[iv].update(neuron_record.times, neuron_record.data, xlabel="x coord",
                                                   ylims=ylims)
                        elif neuron_record.plot_type == "slice_y":
                            neuron_record.times = np.arange(self.grid_size[1])
                            neuron_record.data = vars_2d[iv][:, neuron_record.neuron_coords[0]]
                            ylims = neuron_record.data.min(), neuron_record.data.max()
                            mpl_widgets[iv].update(neuron_record.times, neuron_record.data, xlabel="y coord",
                                                   ylims=ylims)
                        elif neuron_record.plot_type == "radial_average":
                            # neuron_coords = neuron_idx % self.grid_size[0], neuron_idx // self.grid_size[0]
                            neuron_record.data = normed_angular_integral(vars_2d[iv].T, neuron_record.neuron_coords,
                                                                         r_bin=1)
                            neuron_record.times = np.arange(len(neuron_record.data))
                            mpl_widgets[iv].update(neuron_record.times, neuron_record.data, xlabel="radius")

            # convert arrays to bitmaps
            cur_min, cur_max = min(cur_min, current_currents.min()), max(cur_min, current_currents.max())
            pot_min, pot_max = min(pot_min, current_pots.min()), max(pot_min, current_pots.max())

            spike_data = ndarray_to_img(spike_array_2d, amin=0, amax=1, cmap=None)
            pot_data = ndarray_to_img(pot_array_2d, amin=pot_min, amax=pot_max, cmap=cm.viridis)
            current_data = ndarray_to_img(current_array_2d, amin=cur_min, amax=cur_max, cmap=cm.viridis)
            img_arrays = [spike_data, pot_data, current_data]
            imgs_wx = [wx.Image(spike_array_2d.shape[1], spike_array_2d.shape[0], img_array) for img_array in
                       img_arrays]
            imgs_wx = [img_wx.Scale(*self.display_size) for img_wx in imgs_wx]
            img_bitmaps = [wx.Bitmap(img_wx) for img_wx in imgs_wx]

            if len(ffmpegs) > 0:
                for wi, save_anim in enumerate(save_anims):
                    pil_image = Image.fromarray(img_arrays[wi])
                    pil_image = pil_image.resize(self.display_size, PIL.Image.NEAREST)
                    ffmpegs[wi].add_frame(pil_image)

            # update drawn bitmaps
            for i, widget in enumerate(state_widgets):
                widget.bitmap = img_bitmaps[i]
                if self.neuron_record[i] is not None:
                    # selected_neuron_grid_pos = self.neuron_record[i].neuron_idx % self.grid_size[0], \
                    #                           self.neuron_record[i].neuron_idx // self.grid_size[0]
                    widget.plot_type = self.neuron_record[i].plot_type
                    widget.selected_neuron_pos = grid_to_display(*self.neuron_record[i].neuron_coords,
                                                                 self.grid_size, self.display_size)
                    # print(widget.selected_neuron_pos)
                widget.parent.Refresh()

            # add periodic inputs
            if (self.input_period > 0 and sim_t % self.input_period == 0) or (self.input_period == 0 and sim_t == 0):
                # print("ext input dtype: ", ext_input.dtype)
                # print("ext input shape: ", ext_input.shape)
                # print("ext input flags: ", ext_input.flags)
                self.simulation_generator.add_input(ext_input)
            if (self.input_period2 > 0 and sim_t % self.input_period2 == 0) or (self.input_period2 == 0 and sim_t == 0):
                # print("ext input2 dtype: ", ext_input2.dtype)
                # print("ext input2 shape: ", ext_input2.shape)
                # print("ext input2 flags: ", ext_input2.flags)
                self.simulation_generator.add_input(ext_input2)

            sim_t += 1
            time_text.SetLabel("Time step = {}".format(sim_t))
            self.m_statusBar1.SetStatusText("Time step = {}".format(sim_t), 2)

        # save movies
        assert isinstance(settings["fps"], int)
        for i in range(len(save_anims)):
            if save_anims[i]:
                ffmpegs[i].save(anim_paths[i], fps=settings["fps"])

        # plot_step_times = False
        # if plot_step_times:
        #     import matplotlib.pyplot as plt
        #     plt.hist(step_times[2:])
        #     plt.savefig("step_times_{}.png".format(simulation_name))
        return


if __name__ == "__main__":
    app = wx.App()
    frame = INNSFrame(None)
    frame.Show()
    # frame.SetPosition((-1000, 0))
    frame.Maximize(True)
    app.MainLoop()
