import os
import shutil
import subprocess
import io
from datetime import datetime
from PIL import Image
from tempfile import mkdtemp
import logging

from matplotlib.pyplot import Figure


class FFMPEGVideo(object):
    def __init__(self, name: str = None):
        super(FFMPEGVideo, self).__init__()
        self._ffmpeg = shutil.which('ffmpeg')
        self._name = name
        self._workdir = mkdtemp(prefix='FFMPEGVideo.', dir=os.getcwd())
        self._framecounter = 0

        assert self._ffmpeg is not None  # should add ffmpeg\bin directory to PATH

    def __len__(self):
        return self._framecounter

    def add_frame(self, frame):
        filename = os.path.join(self._workdir, 'frame_{:08d}.png'.format(self._framecounter))

        if isinstance(frame, Figure):
            frame.savefig(filename, transparent=False)  # ffmpeg cannot deal properly with transparent pngs
        else:
            frame.save(filename)

        self._framecounter += 1

    def save(self, filename: str = None, fps: int = 30, keep_frame_images=False):
        if self._framecounter == 0:
            raise Exception('No frames stored.')

        if filename is None:
            assert self._name is not None
            filename = self._name

        if not filename.lower().endswith('.mp4'):
            filename += '.mp4'

        print("running ffmpeg")
        ffmpeg = subprocess.run([self._ffmpeg,
                                 '-y',  # force overwrite if output file exists
                                 '-framerate', '{}'.format(fps),
                                 '-i', os.path.join(self._workdir, 'frame_%08d.png'),
                                 '-c:v', 'libx264',
                                 '-preset', 'slow',
                                 '-crf', '17',
                                 '-vf', 'pad=ceil(iw/2)*2:ceil(ih/2)*2,format=yuv420p',
                                 filename,
                                 ])

        if ffmpeg.returncode != 0:
            print("error")
            logging.error('Running the following command failed with return code {}:\n\t{}'
                          .format(ffmpeg.returncode, ' '.join(ffmpeg.args)))
        elif not keep_frame_images:
            shutil.rmtree(self._workdir)
