//cppimport

#include <iostream>
#include <chrono>
#include <numeric>
#include <tuple>
#include <vector>

#include <pybind11/pybind11.h>            // Pybind11 import to define Python bindings

#define FORCE_IMPORT_ARRAY
#include <xtensor/xmath.hpp>              // xtensor import for the C++ universal functions
#include <xtensor-python/pyarray.hpp>     // Numpy bindings
#include <xtensor/xarray.hpp>
#include <xtensor/xio.hpp>
#include <xtensor/xview.hpp>
#include <xtensor/xpad.hpp>
#include <xtensor/xsort.hpp>
#include <xtensor/xmanipulation.hpp>
#include <xtensor/xmasked_view.hpp>
#include <xtensor/xindex_view.hpp>
#include <xtensor/xadapt.hpp>

namespace py = pybind11;
using namespace pybind11::literals;

using namespace xt::placeholders;

using namespace std::chrono;

class LIFSimulation {
public:
    int time_step;
    int num_neurons;
    float dt;
    xt::xarray<float> weight_matrix;
    xt::xarray<int> delay_matrix;
    float v_r;
    float v_t;
    float v_l;
    int tau_ref;
    float tau_mem;
    float cap_mem;
    int synaptic_kernel;
    float tau_exc;
    float tau_inh;
    float tau_cutoff;

    xt::xarray<float> v;
    xt::xarray<int> refractory;

    std::vector<xt::xarray<float>> input_cache;

    xt::xarray<bool> connection_mask;
    xt::xarray<int, xt::layout_type::dynamic> full_targets;
    int current_buffer_pos = 0;
    int max_delay;
    std::vector<std::vector<std::pair<int, float>>> spike_ring_buffer;
    
    LIFSimulation(const int &num_neurons,
        const float &dt,
        const xt::pyarray<float> &weight_matrix,
        const xt::pyarray<int> &delay_matrix,
        const float &v_r,
        const float &v_t,
        const float &v_l,
        const xt::pyarray<float> &v0,
        const xt::pyarray<float> &ext_input,
        const int &tau_ref,
        const float &tau_mem,
        const float &cap_mem,
        const int &synaptic_kernel,
        const float &tau_exc,
        const float &tau_inh,
        const float &tau_cutoff) :
        num_neurons(num_neurons),
        dt(dt),
        weight_matrix(weight_matrix),
        delay_matrix(delay_matrix),
        v_r(v_r),
        v_t(v_t),
        v_l(v_l),
        tau_ref(tau_ref),
        tau_mem(tau_mem),
        cap_mem(cap_mem),
        synaptic_kernel(synaptic_kernel),
        tau_exc(tau_exc),
        tau_inh(tau_inh),
        tau_cutoff(tau_cutoff)
    {
        // internal variables
        time_step = 0;
        refractory = xt::zeros<int>({ (size_t)num_neurons });
        if (!xt::any(xt::isnan(v0))) {
            v = v0;
        } else {
            v = xt::zeros<float>({ (size_t)num_neurons });
        }

        // allow initial external input
        if (!xt::any(xt::isnan(ext_input))) {
            input_cache.push_back(ext_input);
        }

        // delay processing
        connection_mask = xt::not_equal(weight_matrix, 0.0);
        xt::xarray<int> index_cols = xt::tile(xt::arange(num_neurons), num_neurons);
        index_cols.reshape({ (size_t)num_neurons, (size_t)num_neurons });
        full_targets = xt::transpose(index_cols);
        current_buffer_pos = 0;
        if (synaptic_kernel == 0) { // delta
            max_delay = xt::amax(delay_matrix)(0);
        } else if (synaptic_kernel == 1) { // exp
            max_delay = std::max(xt::amax(delay_matrix)(0), std::max(static_cast<int>(tau_cutoff * tau_exc), static_cast<int>(tau_cutoff * tau_inh)));
        }
        spike_ring_buffer.resize(max_delay + 1);
    }

    void LIFSimulation::add_input(xt::pyarray<float> input);

    std::tuple<xt::pyarray<bool>, xt::pyarray<float>, xt::pyarray<float>> LIFSimulation::step(const py::kwargs &kwargs);
};

void LIFSimulation::add_input(xt::pyarray<float> input) {
    if (!xt::any(xt::isnan(input))) {
        this->input_cache.push_back(input);
    }
}

std::tuple<xt::pyarray<bool>, xt::pyarray<float>, xt::pyarray<float>> LIFSimulation::step(const py::kwargs &kwargs) {
    high_resolution_clock::time_point input_start, input_stop, spike_start, spike_stop, delay_start, delay_stop, int_start, int_stop;
    bool debug = false;
    if (kwargs) {
        for (std::pair<py::handle, py::handle> item : kwargs) {
                auto key = item.first.cast<std::string>();
                if (key == "debug") {
                    bool debug_kwargs = item.second.cast<bool>();
                    debug = debug_kwargs;
                }
        }
    }
    if (debug) {
        std::cout << "step " << this->time_step << std::endl;

        input_start = high_resolution_clock::now();
    }

    // get external current signal

    xt::xarray<float> external_input = xt::zeros<float>({ (size_t)this->num_neurons });
    for (size_t i = 0; i < this->input_cache.size(); ++i) {
        // add first column of the input matrix of shape (num_neurons, input_time_steps)
        xt::xarray<float> ext_input = this->input_cache.back();
        xt::xarray<float> ext_input_now = xt::view(ext_input, xt::all(), 0);
        external_input += ext_input_now;
        if (ext_input.shape()[1] > 1) { // the input continues into the next time step
            // re-insert the remaining input time steps
            xt::xarray<float> reduced_ext_input = xt::view(ext_input, xt::all(), xt::range(1, _));
            this->input_cache.insert(this->input_cache.begin(), reduced_ext_input);
        }
        this->input_cache.pop_back(); // remove the input matrix from cache
    }

    if (debug) {
        input_stop = high_resolution_clock::now();
        std::cout << "input handling duration [us]: " << duration_cast<microseconds>(input_stop - input_start).count() << std::endl;

        spike_start = high_resolution_clock::now();
    }

    // spike mechanism
    xt::xarray<bool> spike_mask = (this->v >= this->v_t) && !(this->refractory > 0); // neurons that have spiked
    auto spiked_v = xt::filter(this->v, spike_mask); // view of membrane voltages to be reset
    spiked_v = this->v_r; // reset membrane voltages where neurons have spiked
    auto spiked_ref = xt::filter(this->refractory, spike_mask); // view of refractory times to be started
    spiked_ref = this->tau_ref; // start refractory periods

    if (debug) {
        spike_stop = high_resolution_clock::now();
        auto spike_dur = duration_cast<microseconds>(spike_stop - spike_start);
        std::cout << "spike mechanism duration [us]: " << spike_dur.count() << std::endl;

        delay_start = high_resolution_clock::now();
    }

    // delay processing
    // add new spikes into the spike ring buffer
    if (xt::any(spike_mask)) {
        for (int i = 0; i < this->num_neurons; ++i) {
            if (spike_mask(i)) {
                for (int o = 0; o < this->num_neurons; ++o) {
                    if (this->weight_matrix(o, i) != 0.0) {
                        int delay = this->delay_matrix(o, i);
                        int ring_buffer_pos = (this->current_buffer_pos + delay - 1) % (this->max_delay + 1);
                        int target = this->full_targets(o, i);
                        float weight = this->weight_matrix(o, i);
                        if (this->synaptic_kernel == 0) {
                            this->spike_ring_buffer[ring_buffer_pos].push_back(std::pair<int, float>({target, weight}));
                        } else if (this->synaptic_kernel == 1) {
                            // calculate exponential synaptic kernel
                            float tau_syn;
                            if (weight >= 0) {
                                tau_syn = this->tau_exc;
                            } else if (weight < 0) {
                                tau_syn = this->tau_inh;
                            }
                            // inject currents up to tau_cutoff time constants
                            xt::xarray<int> ts = xt::arange(static_cast<int>(tau_syn * this->tau_cutoff));
                            // full weight decays exponentially with tau
                            xt::xarray<float> weight_kernel = weight / tau_syn * xt::exp(-ts / tau_syn);
                            for (int t = 0; t < weight_kernel.size(); ++t) {
                                int shifted_buffer_pos = (ring_buffer_pos + t) % (this->max_delay + 1);
                                this->spike_ring_buffer[shifted_buffer_pos].push_back(std::pair<int, float>({target, weight_kernel(t)}));
                            }
                        }
                    }
                }
            }
        }
    }
    // read out spike input to be integrated in this time step
    xt::xarray<float> spike_input = xt::zeros<float>({ (size_t)this->num_neurons });
    std::vector<std::pair<int, float>> current_spikes = this->spike_ring_buffer[this->current_buffer_pos];
    if (current_spikes.size() > 0) {
        for (auto p : current_spikes) {
            spike_input(p.first) += p.second;
        }
    }
    // clear the spike entries and advance the buffer position
    this->spike_ring_buffer[this->current_buffer_pos].clear();
    this->current_buffer_pos = (this->current_buffer_pos + 1) % (this->max_delay + 1);

    if (debug) {
        delay_stop = high_resolution_clock::now();
        std::cout << "delay processing duration [us]: " << duration_cast<microseconds>(delay_stop - delay_start).count() << std::endl;

        int_start = high_resolution_clock::now();
    }

    // integration

    // define current input
    xt::xarray<float> current = external_input + spike_input;
    // only update membrane potentials of non-refractory neurons
    xt::xarray<bool> refractory_mask = this->refractory > 0;
    xt::xarray<bool> update_mask = !refractory;
    auto v_update = xt::filter(this->v, update_mask);
    auto c_update = xt::filter(current, update_mask);
    // Euler integration step for the LIF equation
    v_update += ((this->v_l - v_update) / this->tau_mem + c_update / this->cap_mem);
    // without dt -> tau_mem and cap_mem are implicitly measured in dt

    // reduce remaining refractory durations
    auto refractory_masked = xt::filter(this->refractory, refractory_mask);
    refractory_masked -= 1;

    if (debug) {
        int_stop = high_resolution_clock::now();
        auto int_dur = duration_cast<microseconds>(int_stop - int_start);
        std::cout << "integration step duration [us]: " << int_dur.count() << std::endl;
    }
    this->time_step++;

    return std::make_tuple(spike_mask, this->v, current);
}


PYBIND11_MODULE(spinn, m)
{
    xt::import_numpy();

    m.doc() = "Module for simulating spiking neural networks with xtensor python bindings";

    py::class_<LIFSimulation>(m, "LIFSimulation")
        .def(py::init<const int&,
                      const float&,
                      const xt::pyarray<float>&,
                      const xt::pyarray<int>&,
                      const float&,
                      const float&,
                      const float&,
                      const xt::pyarray<float>&,
                      const xt::pyarray<float>&,
                      const int&,
                      const float&,
                      const float&,
                      const int&,
                      const float&,
                      const float&,
                      const float&>())
        .def_readwrite("time_step", &LIFSimulation::time_step)
        .def_readwrite("num_neurons", &LIFSimulation::num_neurons)
        .def_readwrite("dt", &LIFSimulation::dt)
        .def_readwrite("weight_matrix", &LIFSimulation::weight_matrix)
        .def_readwrite("delay_matrix", &LIFSimulation::delay_matrix)
        .def_readwrite("v_r", &LIFSimulation::v_r)
        .def_readwrite("v_t", &LIFSimulation::v_t)
        .def_readwrite("v_l", &LIFSimulation::v_l)
        .def_readwrite("tau_ref", &LIFSimulation::tau_ref)
        .def_readwrite("tau_mem", &LIFSimulation::tau_mem)
        .def_readwrite("cap_mem", &LIFSimulation::cap_mem)
        .def_readwrite("synaptic_kernel", &LIFSimulation::synaptic_kernel)
        .def_readwrite("tau_exc", &LIFSimulation::tau_exc)
        .def_readwrite("tau_inh", &LIFSimulation::tau_inh)
        .def_readwrite("tau_cutoff", &LIFSimulation::tau_cutoff)
        .def_readwrite("v", &LIFSimulation::v)
        .def_readwrite("refractory", &LIFSimulation::refractory)
        .def_readwrite("input_cache", &LIFSimulation::input_cache)
        .def_readwrite("connection_mask", &LIFSimulation::connection_mask)
        .def_readwrite("full_targets", &LIFSimulation::full_targets)
        .def_readwrite("current_buffer_pos", &LIFSimulation::current_buffer_pos)
        .def_readwrite("max_delay", &LIFSimulation::max_delay)
        .def_readwrite("spike_ring_buffer", &LIFSimulation::spike_ring_buffer)
        .def("step", &LIFSimulation::step, py::return_value_policy::move)
        .def("add_input", &LIFSimulation::add_input, py::keep_alive<1, 2>());
}

/*
<%
import os
conda_prefix = os.environ["CONDA_PREFIX"]
#cfg["sources"] = ["spinn.cpp"]
cfg["include_dirs"] = [os.path.join(conda_prefix, "Library", "include"),
					   os.path.join(conda_prefix, "Lib", "site-packages", "numpy", "core", "include"),
					  ]
setup_pybind11(cfg)
%>
*/